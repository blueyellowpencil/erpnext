#!/bin/bash
{
	benchBranch=master
	benchFolderName=bench
	benchFolderName=bench
	benchPath=bench-repo
	benchRepo="https://github.com/frappe/bench"
	erpnextRepo="https://github.com/frappe/erpnext"
	frappeRepo="https://github.com/frappe/frappe"
	pythonVersion=python3
	systemUser=frappe

	adminPass=$ADMIN_PASS
	mysqlPass=$MYSQL_PWD
	siteName=$SITENAME

	echo "=========================================================================================="
	echo "xtop : $(date)"
	echo "=========================================================================================="

	###############################################
	# INSTALL FRAPPE
	###############################################
	#sudo service mysql start
	#mysql --user="root" --execute="ALTER USER 'root'@'localhost' IDENTIFIED BY '${mysqlPass}';"

	###############################################
	# install bench
	###############################################
	# clone & install
	cd /tmp
	git clone --branch $benchBranch --depth 1 --origin upstream $benchRepo $benchPath
	sudo pip3 install -e $benchPath
	bench init $benchFolderName --frappe-path $frappeRepo --frappe-branch version-13 --python $pythonVersion
	# cd into bench folder
	cd $benchFolderName
	# install erpnext
	bench get-app erpnext $erpnextRepo --branch version-13
	# delete temp file
	#sudo rm -rf /tmp/*
	# start new site
	bench new-site $siteName \
		--admin-password $adminPass \
		--mariadb-root-username $MYSQL_USER \
		--mariadb-root-password $mysqlPass \
		--db-name $DB_NAME \
		--db-password $DB_PASS \
		--db-host $MYSQL_HOST \
		--db-port 3306 \
		--force \
		--verbose
		#--no-mariadb-socket \
	bench --site $siteName install-app erpnext
	# compile all python file
	## the reason for not using python3 -m compileall -q /home/$systemUser/$benchFolderName/apps
	## is to ignore frappe/node_modules folder since it will cause syntax error
	$pythonVersion -m compileall -q /home/$systemUser/$benchFolderName/apps/frappe/frappe
	$pythonVersion -m compileall -q /home/$systemUser/$benchFolderName/apps/erpnext/erpnext

	#sudo service mysql start
	bench start
} || {
	echo "============================================="
	echo "ERROR: entrypoint command failed to start"
	echo "============================================="
	tail -f /dev/null
}
